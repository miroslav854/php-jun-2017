<?php

require_once './User.class.php';

if ( !isset($_GET['id']) ) {
  header('Location: ./products.php');
}

require_once './Product.class.php';
$p = new Product($_GET['id']);

if ( isset($_GET['delete']) ) {
  if ( User::isAdmin() ) {
    $p->delete();
    header('Location: ./products.php');
  }
}

?>
<?php include './header.layout.php'; ?>

<h1><?= $p->title ?></h1>

<div class="row mt-5">
  <div class="col-md-5">
    <img src="<?= ($p->image) ? $p->image : './img/product.png' ?>" class="img-fluid" />
  </div>
  <div class="col-md-7">
    <h2>Price</h2>
    <p>&euro; <?= $p->price ?></p>
    <h2  class="mt-5">Description</h2>
    <p><?= $p->description ?></p>
    <div class="clearfix mt-5">
      <button class="btn btn-success float-right">Add to cart</button>
      <?php if (User::isAdmin()): ?>
      <a href="./update-product.php?id=<?= $p->id ?>" class="btn btn-warning float-right">Update product</a>
      <a href="./product-details.php?id=<?= $p->id ?>&delete" class="btn btn-danger float-right">Delete product</a>
      <?php endif; ?>
    </div>
  </div>
</div>

<div class="row mt-5">
  <div class="col-md-12">
    <h3>Comments</h3>
  </div>
</div>

<?php include './footer.layout.php'; ?>