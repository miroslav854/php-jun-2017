<?php

class Product {
  private $db;
  private $config;
  private $image_path = './img/products/';
  private $allowed_image_extensions;
  private $max_image_size;
  public $id;
  public $cat_id;
  public $title;
  public $description;
  public $price;
  public $image;
  public $category;
  public $image_info;

  function __construct($id = null) {
    $this->config = require './config.inc.php';
    $this->db = require './db.inc.php';

    $this->max_image_size = 2 * 1024 * 1024;
    $this->allowed_image_extensions = [
      'image/jpeg', 'image/png', 'image/gif'
    ];
    
    if ( $id != null ) {
      $q_getProductInfo = $this->db->prepare("
        SELECT *
        FROM `products`
        WHERE `id` = :id
      ");
      $q_getProductInfo->bindParam(':id', $id);
      $q_getProductInfo->execute();
      $productInfo = $q_getProductInfo->fetch();

      $this->id = $productInfo['id'];
      $this->cat_id = $productInfo['cat_id'];
      $this->title = $productInfo['title'];
      $this->description = $productInfo['description'];
      $this->price = $productInfo['price'];
      $this->image = $productInfo['image'];
    }
  }

  public function save() {
    if ($this->id == NULL) {
      return $this->insert();
    } else {
      return $this->update();
    }
  }

  public function all() {
    $q_getAllProducts = $this->db->prepare("
      SELECT *
      FROM `products`
    ");
    $q_getAllProducts->execute();
    return $q_getAllProducts->fetchAll();
  }

  public function insert() {
    $this->handleDirectories();
    $q_insertProduct = $this->db->prepare("
      INSERT INTO `products`
      (`cat_id`, `title`, `description`, `price`, `image`)
      VALUES
      (:cat_id, :title, :description, :price, :image)
    ");
    $q_insertProduct->bindParam(":cat_id", $this->cat_id);
    $q_insertProduct->bindParam(":title", $this->title);
    $q_insertProduct->bindParam(":description", $this->description);
    $q_insertProduct->bindParam(":price", $this->price);
    $q_insertProduct->bindParam(":image", $this->image);
    $result = $q_insertProduct->execute();
    $this->id = $this->db->lastInsertId();

    if ( $result && $this->image_info != null ) {
      $fileNameArray = explode('.', $this->image_info['name']);
      $imageExt = strtolower( end( $fileNameArray ) );
      $imagePath = $this->image_path . $this->id . '.' . $imageExt;

      if ( !in_array( $this->image_info['type'], $this->allowed_image_extensions ) ) {
        return false;
      }

      if ( $this->image_info['size'] > $this->max_image_size ) {
        return false;
      }

      move_uploaded_file($this->image_info['tmp_name'], $imagePath);
      
      $this->image = $imagePath;
      $this->save();
      $this->image_info = null;
    }

    return $result;
  }

  public function update() {
    $q_updateProduct = $this->db->prepare("
      UPDATE `products`
      SET
        `cat_id` = :cat_id,
        `title` = :title,
        `description` = :description,
        `price` = :price,
        `image` = :image
      WHERE `id` = :id
    ");
    $q_updateProduct->bindParam(":id", $this->id);
    $q_updateProduct->bindParam(":cat_id", $this->cat_id);
    $q_updateProduct->bindParam(":title", $this->title);
    $q_updateProduct->bindParam(":description", $this->description);
    $q_updateProduct->bindParam(":price", $this->price);
    $q_updateProduct->bindParam(":image", $this->image);
    $result = $q_updateProduct->execute();

    if ( $result && $this->image_info != null && $this->image_info['error'] == 0 ) {
      $fileNameArray = explode('.', $this->image_info['name']);
      $imageExt = strtolower( end( $fileNameArray ) );
      $imagePath = $this->image_path . $this->id . '.' . $imageExt;

      if ( !in_array( $this->image_info['type'], $this->allowed_image_extensions ) ) {
        return false;
      }

      if ( $this->image_info['size'] > $this->max_image_size ) {
        return false;
      }

      $moving_image = move_uploaded_file($this->image_info['tmp_name'], $imagePath);

      $img_update = $this->update_image($imagePath);
      $this->image_info = null;
    }

    return $result;
  }

  public function update_image($imagePath) {
    $q_updateProduct = $this->db->prepare("
      UPDATE `products`
      SET
        `image` = :image
      WHERE `id` = :id
    ");
    $q_updateProduct->bindParam(":id", $this->id);
    $q_updateProduct->bindParam(":image", $imagePath);
    return $q_updateProduct->execute();
  }

  public function delete() {
    if ($this->id != null) {
      $q_deleteProduct = $this->db->prepare("
        DELETE
        FROM `products`
        WHERE `id` = :id
      ");
      $q_deleteProduct->bindParam(':id', $this->id);
      $result = $q_deleteProduct->execute();
      $this->id = null;
      return $result;
    }
  }

  public function search($query) {
    $query = '%' . $query . '%';
    $q_search = $this->db->prepare("
      SELECT *
      FROM `products`
      WHERE `title` LIKE :query_title
      OR `description` LIKE :query_description
    ");
    $q_search->bindParam(':query_title', $query);
    $q_search->bindParam(':query_description', $query);
    $q_search->execute();
    return $q_search->fetchAll();
  }

  private function handleDirectories() {
    if ( !file_exists($this->image_path) ) {
      mkdir($this->image_path, 0777, true);
    }
  }
}