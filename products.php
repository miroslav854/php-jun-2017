<?php

if ( isset($_GET['cat_id']) ) {
  require_once './Category.class.php';
  $c = new Category($_GET['cat_id']);
  $products = $c->products();
  $page_title = $c->title;
} else if( isset($_GET['search']) ) {
  require_once './Product.class.php';
  $p = new Product();
  $products = $p->search($_GET['search']);
  $page_title = 'Search results for "' . $_GET['search'] . '"';
} else {
  require_once './Product.class.php';
  $p = new Product();
  $products = $p->all();
  $page_title = "All products";
}

?>

<?php include './header.layout.php'; ?>

<h1><?= $page_title ?></h1>

<div class="row">
  <?php foreach($products as $product): ?>
    <div class="col-md-4 mt-3">

      <div class="card">
        <span class="badge badge-secondary price">
          <?= $product['price'] ?>
        </span>
        <img class="card-img-top" src="<?= ($product['image']) ? $product['image'] : './img/product.png' ?>">
        <div class="card-body">
          <h4 class="card-title">
            <?= $product['title'] ?>
          </h4>
          <a href="./product-details.php?id=<?= $product['id'] ?>" class="card-link float-right">Details</a>
          <a href="#" class="card-link">Add to cart</a>
        </div>
      </div>
      
    </div>
  <?php endforeach; ?>
</div>

<?php include './footer.layout.php'; ?>